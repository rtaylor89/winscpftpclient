﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectToHSBFTP
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Data.SqlTypes;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Configuration;
         
    internal static class FTPIntegrationDatabase
    {
        public static string SQLConnectionString { get; set; }
        
        private static void ExecuteNonQuery(string sqlQuery, string methodDescription)
        {
            SqlConnection connection = null;
            try
            {
                using (connection = new SqlConnection(SQLConnectionString))
                {
                    SqlCommand command1 = new SqlCommand(sqlQuery, connection);
                    command1.Connection.Open();
                    command1.ExecuteNonQuery();
                    command1.Connection.Close();
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Error Executing Sql Statement: " + methodDescription + " -- " + exception.Message);
            }
        }

        private static string ExecuteScalar(string sqlQuery, string methodDescription)
        {
            SqlConnection connection = null;
            string str;
            try
            {
                using (connection = new SqlConnection(SQLConnectionString))
                {
                    SqlCommand command1 = new SqlCommand(sqlQuery, connection);
                    command1.Connection.Open();
                    object obj2 = command1.ExecuteScalar();
                    command1.Connection.Close();
                    str = (obj2 == null) ? "" : obj2.ToString().Trim();
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Error Executing Sql Statement: " + methodDescription + " -- " + exception.Message);
            }
            return str;
        }

        public static void ExportReportImage(string sqlQuery, string methodDescription)
        {
            SqlConnection connection = null;
            try
            {
                using (connection = new SqlConnection(SQLConnectionString))
                {
                    SqlCommand command1 = new SqlCommand(sqlQuery, connection);
                    command1.Connection.Open();
                    command1.ExecuteNonQuery();
                    command1.Connection.Close();
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Error Executing Sql Statement: " + methodDescription + " -- " + exception.Message);
            }
        }

        public static string GetApplicationSetting(string name) =>
            ExecuteScalar("select Value from Settings where Name = '" + name + "'", GetCurrentMethod());

        public static DataTable GetApplicationSettings()
        {
            SqlConnection selectConnection = null;
            SqlDataAdapter adapter = null;
            string selectCommandText = "select * from Settings";
            DataTable dataTable = new DataTable();
            try
            {
                using (selectConnection = new SqlConnection(SQLConnectionString))
                {
                    using (adapter = new SqlDataAdapter(selectCommandText, selectConnection))
                    {
                        adapter.Fill(dataTable);
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Error Executing Sql Statement: " + selectCommandText + " -- " + exception.Message);
            }
            return dataTable;
        }

        public static DataTable GetSiteSettings(int siteId)
        {
            SqlConnection selectConnection = null;
            SqlDataAdapter adapter = null;
            string selectCommandText = "select * from Sites where Id = " + siteId;
            DataTable dataTable = new DataTable();
            try
            {
                using (selectConnection = new SqlConnection(SQLConnectionString))
                {
                    using (adapter = new SqlDataAdapter(selectCommandText, selectConnection))
                    {
                        adapter.Fill(dataTable);
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Error Executing Sql Statement: " + selectCommandText + " -- " + exception.Message);
            }
            return dataTable;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static string GetCurrentMethod() =>
            new StackTrace().GetFrame(1).GetMethod().Name;

        
        public static string GetSQLServerName()
        {
            using (SqlConnection connection = new SqlConnection(SQLConnectionString))
            {
                return connection.DataSource;
            }
        }

        public static void InsertLogMessage(string message, string moduleName, string userId, string machineName, int logLevel)
        {

            string sqlQuery = "insert into ApplicationLog (Message,ModuleName,UserId,MachineName,LogLevel) " +
                              "values ('" + message + "','" + moduleName + "','" + userId + "','" + machineName + "'," + logLevel.ToString() + ")";

            ExecuteNonQuery(sqlQuery, GetCurrentMethod());

        }

        public static void UpdateAllocationTable(string allocationId, string sageAcctCode)
        {
            string[] textArray1 = new string[] { "update [ReportAllocations] set SageAccountCode = '", sageAcctCode, "'where AllocationID = '", allocationId, "'" };
            ExecuteNonQuery(string.Concat(textArray1), GetCurrentMethod());
        }
                

        public static void UpdateApplicationSetting(string settingName, string value)
        {
            string sqlQuery = "update Settings set Value = '" + value + "' where Name = '" + settingName + "'";

            ExecuteNonQuery(sqlQuery, GetCurrentMethod());
        }       
        
    }
}







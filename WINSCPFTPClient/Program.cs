using ConnectToHSBFTP;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Runtime.CompilerServices;


class WinFTPConnectToHSB
{
    private enum LogLevel
    {
        Debug = 1,
        Informational = 2,
        Error = 3
    }

    public static int Main(string[] args)
    {
        string _config_SQLServerConnectionString = "";
        int     _config_SiteID = -1;
        int    _config_logLevel = 3;
        bool   _config_enableEmail = false;
        string _config_smtpFrom = "";
        string _config_smtpTo = "";
        string _config_smtpServer = "";
        string _config_smtpSubject = "";
        int    _config_smtpPort = 25;
        string _config_smtpUserName = "";
        string _config_smtpUserPassword = "";
        bool   _config_useSSL = true;
        string _config_logFilePrefix = "FTP";
        string _config_fileExportPath = "";
        string _config_logFileDirectory = "";        
        string _config_UserName = "";
        string _config_Password = "";        
        string _config_BatchFile = "";
        string _config_SeedValue = "";
        int     numberOfArguments = -1;


        try
        {
            
            numberOfArguments = args.Length;

            //Get Application Settings, i.e. DatabaseConnection String, SiteId

            Console.WriteLine("Getting Application Settings");
            GetApplicationSettings();
            FTPIntegrationDatabase.SQLConnectionString = _config_SQLServerConnectionString;

            //Console.WriteLine("The SQL ConnectionString -- " + _config_SQLServerConnectionString);

            LogMessage("The number of arguments -- " + numberOfArguments.ToString(), LogLevel.Informational, GetCurrentMethod(), true);
            
            if (numberOfArguments > 0)
            {
                _config_SiteID = Convert.ToInt32(args[0].ToString());
            }
            else
            {
                Console.WriteLine($"Usage: ConnectToHSBFTP SiteId=#");
            }

            LogMessage("The SiteID is -- " + _config_SiteID, LogLevel.Informational, GetCurrentMethod(), true);

            

            //Get Application Settings from Database
            LoadApplicationSettings();

            //Get Site Settings from Database
            LoadSiteSettings();

            Console.WriteLine("The Siteid -- " + _config_SiteID);
            Console.WriteLine("The Batchfile -- " + _config_BatchFile);
            Console.WriteLine("The Arguments -- " + _config_UserName + "," + "the user password");
            Console.WriteLine("The Pwd length -- " + _config_Password.Length.ToString());

            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = _config_BatchFile;
            proc.StartInfo.Arguments = String.Format("{0} {1}", _config_UserName, _config_Password);
            proc.Start();
            proc.WaitForExit();

            Console.WriteLine("Exe Completed");
            return 0;
        }
        catch (Exception e)
        {
            LogMessage("Error: " + e.Message + " -- " + _config_SiteID.ToString(), LogLevel.Error, GetCurrentMethod(), true);            
            return 1;
        }        

        void LoadApplicationSettings()
        {
            DataTable applicationSettings = null;
            string str = "";
            try
            {
                applicationSettings = FTPIntegrationDatabase.GetApplicationSettings();
                if ((applicationSettings == null) || (applicationSettings.Rows.Count <= 0))
                {
                    LogMessage("No Settings Returned From Database", LogLevel.Error, GetCurrentMethod(), false);
                }
                else
                {
                    foreach (DataRow row in applicationSettings.Rows)
                    {
                        str = row["Name"].ToString();                       
                        _config_logLevel = (str == "LogReportLevel") ? Convert.ToInt32(row["Value"].ToString()) : _config_logLevel;
                        _config_enableEmail = (str == "EnableEmail") ? Convert.ToBoolean(row["Value"].ToString()) : _config_enableEmail;
                        _config_smtpFrom = (str == "SMTPFrom") ? row["Value"].ToString() : _config_smtpFrom;
                        _config_smtpTo = (str == "SMTPTo") ? row["Value"].ToString() : _config_smtpTo;
                        _config_smtpServer = (str == "SMTPServer") ? row["Value"].ToString() : _config_smtpServer;
                        _config_smtpSubject = (str == "SMTPSubject") ? row["Value"].ToString() : _config_smtpSubject;
                        _config_smtpPort = (str == "SMTPPort") ? Convert.ToInt32(row["Value"].ToString()) : _config_smtpPort;
                        _config_smtpUserName = (str == "SMTPUserName") ? row["Value"].ToString() : _config_smtpUserName;
                        _config_smtpUserPassword = (str == "SMTPPassword") ? row["Value"].ToString() : _config_smtpUserPassword;
                        _config_useSSL = (str == "SMTPUseSSL") ? Convert.ToBoolean(row["Value"].ToString()) : _config_useSSL;
                        _config_logFilePrefix = (str == "LogFilePrefix") ? row["Value"].ToString() : _config_logFilePrefix;
                        _config_logLevel = (str == "LogReportLevel") ? Convert.ToInt32(row["Value"].ToString()) : _config_logLevel;                        
                        _config_fileExportPath = (str == "JournaExportFilePath") ? row["Value"].ToString() : _config_fileExportPath;


                    }
                }
            }
            catch (Exception exception)
            {
                LogMessage("Error Loading Config Setting From Database " + str + " -- " + exception.Message, LogLevel.Error, GetCurrentMethod(), true);
            }
        }

        void LoadSiteSettings()
        {
            DataTable siteSettings = null;
            string str = "";
            //string seedvalue = "ha7cb5835a4e4177bbce2ea4316j7423";

            try
            {
                siteSettings = FTPIntegrationDatabase.GetSiteSettings(_config_SiteID);
                if ((siteSettings == null) || (siteSettings.Rows.Count <= 0))
                {
                    LogMessage("No Settings Returned From Database", LogLevel.Error, GetCurrentMethod(), false); 
                }
                else
                {
                        DataRow row = siteSettings.Rows[0];
                        _config_UserName  =  row["UserName"].ToString();
                        _config_SeedValue = row["SeedValue"].ToString();
                        _config_Password  =  EncryptDecryptClassLibrary.EncryptDecrypt.DecryptString(_config_SeedValue, row["Password"].ToString());                        
                        _config_BatchFile =  row["BatchFile"].ToString();                        
                }
            }
            catch (Exception exception)
            {
                LogMessage("Error Loading Config Setting From Database " + str + " -- " + exception.Message, LogLevel.Error, GetCurrentMethod(), true);
            }
        }
        

        void LogMessage(string message, LogLevel logLevel, string moduleName, bool inSendEmail = false)
        {
            string sqlFormattedMessage = RemoveSpecialChars(message).Replace("'", "''");
            string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            string machineName = Environment.MachineName;

            if ((int)logLevel >= _config_logLevel)
            {
                if ((int)logLevel >= _config_logLevel)
                {
                    try
                    {
                        FTPIntegrationDatabase.InsertLogMessage(sqlFormattedMessage, moduleName, userName, machineName, (int)logLevel);

                        if(_config_logFileDirectory.Length == 0)
                        {
                            _config_logFileDirectory = @"C:\LogFiles";
                        }

                        Console.WriteLine("Looking for Config Directory -- " + _config_logFileDirectory);
                        if (!Directory.Exists(_config_logFileDirectory))
                        {
                            Console.WriteLine("Creating Config Directory -- " + _config_logFileDirectory);
                            Directory.CreateDirectory(_config_logFileDirectory);
                            Console.WriteLine("Creating Config Directory Successful -- ");
                        }
                    }
                    catch (Exception ex)
                    {
                        StreamWriter sw = null;

                        //Console.WriteLine("Looking for Config Directory -- " + _config_logFileDirectory);
                        //if (!Directory.Exists(_config_logFileDirectory))
                        //{
                        //    Console.WriteLine("Creating Config Directory -- " + _config_logFileDirectory);
                        //    Directory.CreateDirectory(_config_logFileDirectory);
                        //    Console.WriteLine("Creating Config Directory Successful -- ");
                        //}

                        Console.WriteLine("Writing Error to -- " + _config_logFileDirectory);
                        Console.WriteLine("Error is -- " + ex.Message);
                        using (sw = new StreamWriter(_config_logFileDirectory + @"\" + _config_logFilePrefix + "logFile_" + DateTime.Now.Date.ToString("MMddyyyy") + ".log", true))
                        {
                            sw.Write("Original Error -- " + message + "\r\n");
                            sw.Write("Log Write Error -- " + ex.Message + "\r\n");
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }

                if (inSendEmail && _config_enableEmail)
                {
                    SendEmail(message);
                }
            }

        }

        void SendEmail(string inMessage)
        {
            MailMessage mail = null;
            SmtpClient mailClient = null;

            try
            {
                mail = new MailMessage(_config_smtpFrom, _config_smtpTo, _config_smtpSubject, inMessage);
                mailClient = new SmtpClient(_config_smtpServer, _config_smtpPort);
                mailClient.EnableSsl = _config_useSSL;

                if (_config_smtpUserName.Trim() != "")
                {
                    mailClient.Credentials = new NetworkCredential(_config_smtpUserName, _config_smtpUserPassword);
                }

                mailClient.Send(mail);
            }
            catch (SmtpException stex)
            {
                LogMessage("Error Sending Email -- " + stex.Message, LogLevel.Error, GetCurrentMethod(), false);
            }
            catch (Exception ex)
            {
                LogMessage("Unknown Error Sending Email -- " + ex.Message, LogLevel.Error, GetCurrentMethod(), false);
            }
        }

        void GetApplicationSettings()
        {

            var section = ConfigurationManager.GetSection("applicationSettings");

            // Get the AppSettings section.
            NameValueCollection appSettings = ConfigurationManager.AppSettings;

            Console.WriteLine("Getting the SQL ConnectionString");

            _config_SQLServerConnectionString = appSettings.GetValues("SQLConnectionString")[0].ToString();

            Console.WriteLine("The SQL ConnectionString -- " + _config_SQLServerConnectionString);

            Console.WriteLine("Getting the LogFile Directory");
            //_config_SiteID = Convert.ToInt32(appSettings.GetValues("ID")[0].ToString());
            _config_logFileDirectory = appSettings.GetValues("LogFileDirectory")[0].ToString();
            Console.WriteLine("The LogFile Directory -- " + _config_logFileDirectory);

            return;
        }        

        string RemoveSpecialChars(string str)
        {
            string[] strArray = new string[] { "@", "%", "&", "*", "'", ";" };
            for (int i = 0; i < strArray.Length; i++)
            {
                if (str.Contains(strArray[i]))
                {
                    str = str.Replace(strArray[i], "");
                }
            }
            return str;
        }
    }

    [MethodImpl(MethodImplOptions.NoInlining)]
    public static string GetCurrentMethod()
    {
        var st = new StackTrace();
        var sf = st.GetFrame(1);

        return sf.GetMethod().Name;
    }
}




